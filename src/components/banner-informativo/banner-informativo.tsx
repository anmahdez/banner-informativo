import { Component, State, Element, h } from '@stencil/core';


@Component({
  tag: 'banner-informativo',
  assetsDirs: ['assets'],
  styleUrl: 'banner-informativo.css',
  shadow: true,
})

export class BannerInformativo {
  @State() data: any;
  @Element() el: HTMLElement;
  
  async componentWillLoad() {
    await fetch('https://api-interno.www.gov.co/api/home/BannerInformativo')
      .then((response: Response) => response.json())
      .then(response => {
        this.data = response.data;
      });
  }

  componentDidLoad() {
    var document = this.el.shadowRoot.getElementById("banner-back");
    document.style.background = this.data.colorFondo;

    var document = this.el.shadowRoot.getElementById("color-blanco-1");
    document.style.color = this.data.colorTexto;

    var document = this.el.shadowRoot.getElementById("color-blanco-2");
    document.style.color = this.data.colorTexto;

    var document = this.el.shadowRoot.getElementById("color-blanco-3");
    document.style.color = this.data.colorTexto;
  }

  render() {
    return (
      <div>
        <div id="banner-back">
          <div class="container">
            <div class="banner-informativo align-items-center row">
              <div class="col-sm-12 col-md-9">
                <a id="color-blanco-1" class="banner-informativo-text" role="link" href={this.data.urlDestino} rel="noreferrer" target="">
                  <span
                    class="icono-alerta">&nbsp;&nbsp;&nbsp;</span>{this.data.texto}&nbsp;&nbsp;&nbsp;
                </a>
              </div>
              <div class="col-sm-12 col-md-3 banner-informativo-recordarmelo">
                <a href="javascript:void(0)" role="button" id="color-blanco-2">
                  <u id="color-blanco-3">{this.data.textoAuxiliar}</u>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
